import ProjectInfo._
import kevinlee.sbt.SbtCommon.crossVersionProps
import just.semver.SemVer
import SemVer.{Major, Minor}

val ProjectScalaVersion: String = "2.13.1"
val CrossScalaVersions: Seq[String] = Seq("2.10.7", "2.11.12", "2.12.10", ProjectScalaVersion)

val hedgehogVersion = "6dba7c9ba065e423000e9aa2b6981ce3d70b74cb"
val hedgehogRepo: MavenRepository =
  "bintray-scala-hedgehog" at "https://dl.bintray.com/hedgehogqa/scala-hedgehog"

val hedgehogLibs: Seq[ModuleID] = Seq(
    "hedgehog" %% "hedgehog-core" % hedgehogVersion % Test
  , "hedgehog" %% "hedgehog-runner" % hedgehogVersion % Test
  , "hedgehog" %% "hedgehog-sbt" % hedgehogVersion % Test
)

val justFp: ModuleID = "io.kevinlee" %% "just-fp" % "1.3.4"

val argonaut: ModuleID = "io.argonaut" %% "argonaut" % "6.2.3"

val wartRemover: ModuleID = "org.wartremover" % "sbt-wartremover" % "2.2.1"

val scoverage: ModuleID = "org.scoverage" % "sbt-scoverage" % "1.5.1"

ThisBuild / scalaVersion     := ProjectScalaVersion
ThisBuild / version          := ProjectVersion
ThisBuild / organization     := "io.kevinlee"
ThisBuild / developers   := List(
    Developer("Kevin-Lee", "Kevin Lee", "kevin.code@kevinlee.io", url("https://github.com/Kevin-Lee"))
  )
ThisBuild / homepage := Some(url("https://github.com/Kevin-Lee/just-http"))
ThisBuild / scmInfo :=
  Some(ScmInfo(
    url("https://github.com/Kevin-Lee/just-http")
  , "git@github.com:Kevin-Lee/just-http.git"
  ))

lazy val justHttp = (project in file("."))
  .settings(
    name := "just-http"
  , description  := "Just HTTP Client for Scala"
  , crossScalaVersions := CrossScalaVersions
  , resolvers ++= Seq(hedgehogRepo)
  , libraryDependencies ++= hedgehogLibs ++ Seq(justFp, argonaut % Test)
  , dependencyOverrides ++=
      crossVersionProps(Seq.empty, SemVer.parseUnsafe(scalaVersion.value)) {
        case (Major(2), Minor(10)) =>
          Seq(
              "org.wartremover" %% "wartremover" % "2.3.7"
            , "io.argonaut" %% "argonaut" % "6.2.2" % Test
            )
        case x =>
          Seq.empty
      }
  , unmanagedSourceDirectories in Compile ++= {
      val sharedSourceDir = (baseDirectory in ThisBuild).value / "src/main"
      if (scalaVersion.value.startsWith("2.10") || scalaVersion.value.startsWith("2.11"))
        Seq(sharedSourceDir / "scala-2.10_2.11")
      else if (scalaVersion.value.startsWith("2.12") || scalaVersion.value.startsWith("2.13"))
        Seq(sharedSourceDir / "scala-2.12_2.13")
      else
        Seq()
    }
  , unmanagedSourceDirectories in Test ++= {
      val sharedSourceDir = (baseDirectory in ThisBuild).value / "src/test"
      if (scalaVersion.value.startsWith("2.10") ||
          scalaVersion.value.startsWith("2.11") ||
          scalaVersion.value.startsWith("2.12"))
        Seq(sharedSourceDir / "scala-2.10_2.12")
      else
        Seq()
    }
  , fork := true
  , wartremoverErrors in (Compile, compile) ++= commonWarts
  , wartremoverErrors in (Test, compile) ++= commonWarts
  , testFrameworks ++= Seq(TestFramework("hedgehog.sbt.Framework"))

  /* Bintray { */
  , bintrayPackageLabels := Seq("Scala", "HTTP", "HTTP Client")
  , bintrayVcsUrl := Some("""git@github.com:Kevin-Lee/just-http.git""")
  , licenses += ("MIT", url("http://opensource.org/licenses/MIT"))
  /* } Bintray */

  , initialCommands in console := """import kevinlee.http._"""

  /* Coveralls { */
  , coverageHighlighting := (CrossVersion.partialVersion(scalaVersion.value) match {
      case Some((2, 10)) =>
        false
      case _ =>
        true
    })
  /* } Coveralls */

  )

