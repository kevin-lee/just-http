# just-http

[![Actions Status](https://github.com/Kevin-Lee/just-http/workflows/Docker%20Image%20CI/badge.svg)](https://github.com/Kevin-Lee/just-http/actions?workflow=Docker+Image+CI) [![Actions Status](https://github.com/Kevin-Lee/just-http/workflows/CI%20%2B%20Docker%20Services/badge.svg)](https://github.com/Kevin-Lee/just-http/actions?workflow=CI+%2B+Docker+Services) [![Release Status](https://github.com/Kevin-Lee/just-http/workflows/Release/badge.svg)](https://github.com/Kevin-Lee/just-http/actions?workflow=Release)

[![Coverage Status](https://coveralls.io/repos/github/Kevin-Lee/just-http/badge.svg?branch=master)](https://coveralls.io/github/Kevin-Lee/just-http?branch=master)

Just HTTP Client for Scala
