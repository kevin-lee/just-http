#!/bin/bash

set -ex

docker-compose -f .docker/docker-compose.yml run -e GITHUB_REF=${GITHUB_REF} -v ~/.sbt/boot:/root/.sbt/boot -v ~/.ivy2/cache:/root/.ivy2/cache -v ~/.coursier/cache:/root/.coursier/cache app-test /app/.local/sbt-build-local.sh
