#!/bin/bash

set -ex

echo "============================================"
echo "Build projects (Local)"
echo "--------------------------------------------"
echo ""
cd /app
if [ -z "$1" ]
  then
    sbt -d -J-Xmx2048m "; clean; +test"
else
  scala_version=$1
  sbt -J-Xmx2048m "; ++ ${scala_version}!; clean; test"
fi

echo "============================================"
echo "Building projects (Local): Done"
echo "============================================"
