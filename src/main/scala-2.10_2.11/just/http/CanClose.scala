package just.http

/**
  * @author Kevin Lee
  * @since 2019-05-26
  */
trait CanClose[A] {
  def close(a: A): Unit
}

object CanClose {
  implicit def canCloseAutoCloseable[A <: AutoCloseable]: CanClose[A] = new CanClose[A] {
    override def close(a: A): Unit = a.close()
  }
}
