package just.http

import just.fp.syntax.EitherSyntax

/**
 * @author Kevin Lee
 * @since 2019-05-29
 */
trait CrossVersion extends EitherSyntax

object CrossVersion {
  @SuppressWarnings(Array("org.wartremover.warts.PublicInference"))
  val JavaConverters = scala.collection.JavaConverters
}
