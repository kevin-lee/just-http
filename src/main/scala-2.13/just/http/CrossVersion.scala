package just.http

/**
 * @author Kevin Lee
 * @since 2019-05-29
 */
trait CrossVersion

object CrossVersion {
  @SuppressWarnings(Array("org.wartremover.warts.PublicInference"))
  val JavaConverters = scala.jdk.CollectionConverters
}
