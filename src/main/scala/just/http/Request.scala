package just.http

import java.net.{URI, URLEncoder}

import just.fp.compat.EitherCompat

/**
  * @author Kevin Lee
  * @since 2019-04-17
  */
final case class Request(uri: URI, method: Request.Method, headers: Request.Headers)

object Request {

  def get(url: String): Either[HttpError, Request] =
    EitherCompat.map(Connection.uri(url))(uri => Request(uri, Method.get, Headers.empty))

  def withAuth(request: Request, auth: Auth): Request =
    request.copy(headers = Headers.add(request.headers, Auth.toHeader(auth)))

  sealed trait Method

  object Method {
    final case object Get extends Method
    final case object Post extends Method
    final case object Put extends Method
    final case object Delete extends Method
    final case object Head extends Method

    def get: Method = Get
    def post:Method = Post
    def put: Method = Put
    def delete: Method = Delete
    def head: Method = Head

    def render(method: Method): String = method match {
      case Get => "GET"
      case Post => "POST"
      case Put => "PUT"
      case Delete => "DELETE"
      case Head => "HEAD"
    }
  }

  final case class Headers(headers: List[(String, String)])

  object Headers {

    def empty: Headers = Headers(List.empty)

    def add(headers: Headers, header: (String, String)): Headers =
      headers.copy(header :: headers.headers)
  }

  final case class Params(params: List[(String, String)]) extends AnyVal
  object Params {
    def empty: Params = Params(List.empty)
  }

  def urlEncodeWithUtf8(keyValuePairs: List[(String, String)]): List[(String, String)] =
    keyValuePairs.map { case (key, value) =>
      (
        URLEncoder.encode(key, "UTF-8").replaceAllLiterally("+", "%20")
      , URLEncoder.encode(value, "UTF-8").replaceAllLiterally("+", "%20")
      )
    }

  def buildParams(params: Params): String =
    (for {
      (encodedName, encodedValue) <- urlEncodeWithUtf8(params.params)
      if encodedName.nonEmpty
    } yield s"$encodedName=$encodedValue").mkString("&")

}
