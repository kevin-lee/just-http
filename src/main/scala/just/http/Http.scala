package just.http

import java.io.{DataOutputStream, InputStream}
import java.net.URI
import java.nio.charset.StandardCharsets

import Request.Params

/**
  * @author Kevin Lee
  * @since 2019-05-26
  */
object Http extends CrossVersion {

  def request(request: Request, params: Params): Either[HttpError, Response] = request match {
    case Request(uri, method@(Request.Method.Get | Request.Method.Head), headers) =>
      for {
        uriWithParams <- Connection.uriWithParams(uri, params)
        connection <- Connection.toHttpConnection(uriWithParams)
        _ = Connection.setMethod(connection, method)
        _ = Connection.setRequestHeaders(connection, headers)
        response <- Response.fromConnectionToResponse(connection)
      } yield response

    case Request(uri, Request.Method.Post, headers) =>
      processNonGetMethod(uri, NonGetMethod.post, params, headers)

    case Request(uri, Request.Method.Put, headers) =>
      processNonGetMethod(uri, NonGetMethod.put, params, headers)

    case Request(uri, Request.Method.Delete, headers) =>
      processNonGetMethod(uri, NonGetMethod.delete, params, headers)
  }

  def download[A](
    request: Request
  , params: Params)(
    downloadHandler: (URI, Response.Headers, InputStream) => A
  ): Either[HttpError, A] = request match {
    case Request(uri, Request.Method.Get, headers) =>
      for {
        uriWithParams <- Connection.uriWithParams(uri, params)
        connection <- Connection.toHttpConnection(uriWithParams)
        _ = Connection.setMethod(connection, Request.Method.get)
        _ = Connection.setRequestHeaders(connection, headers)
        r <- Response.fromConnection(connection)((_, headers, input) => downloadHandler(uri, headers, input))
      } yield r

    case Request(uri, Request.Method.Post, headers) =>
      processNonGetMethodWithResponseHandler(
        uri, NonGetMethod.post, params, headers)(
        (_, headers, input) => downloadHandler(uri, headers, input)
      )

    case request: Request =>
      Left(HttpError.unsupportedMethod(request))

  }

  private[this] sealed trait NonGetMethod

  private[this] object NonGetMethod {
    final case object Post extends NonGetMethod
    final case object Put extends NonGetMethod
    final case object Delete extends NonGetMethod

    def post: NonGetMethod = Post
    def put: NonGetMethod = Put
    def delete: NonGetMethod = Delete

    def toRequestMethod(postOrPut: NonGetMethod): Request.Method = postOrPut match {
      case NonGetMethod.Post => Request.Method.post
      case NonGetMethod.Put => Request.Method.put
      case NonGetMethod.Delete => Request.Method.delete
    }
  }

  private def processNonGetMethod(
    uri: URI
  , method: NonGetMethod
  , params: Params
  , headers: Request.Headers
  ): Either[HttpError, Response] =
    processNonGetMethodWithResponseHandler(uri, method, params, headers)(Response.toResponse)

  private def processNonGetMethodWithResponseHandler[A](
    uri: URI
  , method: NonGetMethod
  , params: Params
  , headers: Request.Headers)(
    responseHandler: (Status, Response.Headers, InputStream) => A
  ): Either[HttpError, A] =
    Connection.toHttpConnection(uri).flatMap { connection =>
      Connection.setMethod(connection, NonGetMethod.toRequestMethod(method))
      val byteParams = Request.buildParams(params).getBytes(StandardCharsets.UTF_8)
      val length = byteParams.length
      Connection.setRequestHeaders(
        connection, headers.copy(headers.headers ++
          List(
              "Content-Type" -> "application/x-www-form-urlencoded"
            , "charset" -> "UTF-8"
            , "Content-Length" -> length.toString
          )
        )
      )
      connection.httpUrlConnection.setDoOutput(true)
      connection.httpUrlConnection.setInstanceFollowRedirects(false)
      connection.httpUrlConnection.setUseCaches(false)
      IoUtil.tryWith(new DataOutputStream(connection.httpUrlConnection.getOutputStream)) {
        outputStream =>
          outputStream.write(byteParams)
          outputStream.flush()
      }
      Response.fromConnection(connection)(responseHandler)
    }

  def head(url: String, params: Params): Either[HttpError, Response] =
    headWithHeaders(url, Request.Headers.empty, params)

  def headWithHeaders(url: String, headers: Request.Headers, params: Params): Either[HttpError, Response] =
    for {
      uri <- Connection.uri(url)
      response <- request(Request(uri, Request.Method.head, headers), params)
    } yield response

  def get(url: String, params: Params): Either[HttpError, Response] =
    getWithHeaders(url, Request.Headers.empty, params)

  def getWithHeaders(url: String, headers: Request.Headers, params: Params): Either[HttpError, Response] =
    for {
      uri <- Connection.uri(url)
      response <- request(Request(uri, Request.Method.get, headers), params)
    } yield response

  def post(url: String, params: Params): Either[HttpError, Response] =
    postWithHeaders(url, Request.Headers.empty, params)

  def postWithHeaders(url: String, headers: Request.Headers, params: Params): Either[HttpError, Response] =
    for {
      uri <- Connection.uri(url)
      response <- request(Request(uri, Request.Method.post, headers), params)
    } yield response

  def put(url: String, params: Params): Either[HttpError, Response] =
    putWithHeaders(url, Request.Headers.empty, params)

  def putWithHeaders(url: String, headers: Request.Headers, params: Params): Either[HttpError, Response] =
    for {
      uri <- Connection.uri(url)
      response <- request(Request(uri, Request.Method.put, headers), params)
    } yield response

  def delete(url: String, params: Params): Either[HttpError, Response] =
    deleteWithHeaders(url, Request.Headers.empty, params)

  def deleteWithHeaders(url: String, headers: Request.Headers, params: Params): Either[HttpError, Response] =
    for {
      uri <- Connection.uri(url)
      response <- request(Request(uri, Request.Method.delete, headers), params)
    } yield response

}
