package just.http

/**
  * @author Kevin Lee
  * @since 2019-05-25
  */
final case class Status(status: Int)
