package just.http

import java.io.{ByteArrayOutputStream, InputStream}

import Connection.HttpConnection
import CrossVersion.JavaConverters._
import just.fp.syntax._

/**
  * @author Kevin Lee
  * @since 2019-04-17
  */
final case class Response(status: Status, headers: Response.Headers, body: Body)

object Response {

  final case class Headers(headers: List[(String, List[String])]) extends AnyVal

  object Headers {
    def empty: Headers = Headers(List.empty)

    def find(name: String, headers: Headers): Option[List[String]] =
      headers.headers.find{ case (n, _) =>
        Option(n).map(_.toLowerCase) === Option(name).map(_.toLowerCase)
      }.map(_._2)

  }

  def toResponse(status: Status, headers: Headers, input: InputStream): Response =
    IoUtil.tryWith(new ByteArrayOutputStream()) { output =>
      IoUtil.transfer(8192, input, output)
      Response(
          status
        , headers
        , Body(output.toByteArray)
      )
    }

  def fromConnectionToResponse(connection: HttpConnection): Either[HttpError, Response] =
    fromConnection(connection)(Response.toResponse)

  def fromConnection[A](
    connection: HttpConnection)(
    responseHandler: (Status, Headers, InputStream) => A
  ): Either[HttpError, A] = {
    val httpUrlConnection = connection.httpUrlConnection
    val status = Status(httpUrlConnection.getResponseCode)

    status match {
      case Status(200) =>
        IoUtil.tryWith(httpUrlConnection.getInputStream) { input =>
          IoUtil.tryWith(new ByteArrayOutputStream()) { output =>
            val headers =
              httpUrlConnection.getHeaderFields.asScala.toList.map { case (k, v) =>
                (k, v.asScala.toList)
              }
              responseHandler(status, Response.Headers(headers), input).right
          }
        }
      case Status(statusCode) if statusCode / 100 === 3 =>
        // TODO: finish it
        println(
          s"""${statusCode.toString}
             |""".stripMargin)
        ???
      case Status(statusCode) =>
        // TODO: finish it
        val error = Option(httpUrlConnection.getErrorStream)
          .fold("") {
            IoUtil.tryWith(_) { input =>
              IoUtil.tryWith(new ByteArrayOutputStream()) { output =>
                IoUtil.transfer(8192, input, output)
                new String(output.toByteArray)
              }
            }
          }
        println(
          s"""${statusCode.toString}
             |error: $error
             |""".stripMargin)
        ???


    }
  }

}
