package just.http

import java.io.{File, FileOutputStream, InputStream}
import java.net.URI

import just.fp.syntax._

/**
 * @author Kevin Lee
 * @since 2019-10-28
 */
object Download {

  final case class FileDownload(dir: File, filename: Option[String])

  def download(
    download: FileDownload
  , uri: URI
  , headers: Response.Headers
  , bufferSize: Int
  ): InputStream => File =
    { input =>
      val file = new File(
        download.dir, Download.getFilename(download.filename, uri, headers)
      )
      IoUtil.tryWith(new FileOutputStream(file)) { output =>
        IoUtil.transfer(bufferSize, input, output)
        file
      }
    }

  def getFilename(
    filename: Option[String]
  , uri: URI
  , headers: Response.Headers
  ): String =
    filename.orElse(
      Response.Headers.find(Constants.Headers.ContentDisposition, headers)
        .flatMap { values =>
          values.map { value =>
            val start = value.indexOf("filename=")
            if (start >= 0 )
              value.substring("filename=\"".length, value.length - 1).some
            else
              none[String]
          }.find(_.isDefined).flatten
        }
    ).getOrElse {
      val path = uri.getPath
      path.substring(path.lastIndexOf("/") + 1)
    }

}
