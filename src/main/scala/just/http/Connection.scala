package just.http

import java.io.IOException
import java.net._

import javax.net.ssl.HttpsURLConnection

import just.fp.syntax._

import Request.Params

/**
  * @author Kevin Lee
  * @since 2019-05-26
  */
object Connection {

  final case class HttpConnection(httpUrlConnection: HttpURLConnection)

  def httpConnection(httpUrlConnection: HttpURLConnection): HttpConnection =
    HttpConnection(httpUrlConnection)

  def httpsConnection(httpsUrlConnection: HttpsURLConnection): HttpConnection =
    HttpConnection(httpsUrlConnection)

  def uri(uri: String): Either[HttpError, URI] =
    try {
      URI.create(uri).right
    } catch {
      case ex: URISyntaxException =>
        HttpError.uriSyntaxError(uri, ex).left
    }

  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  def uriWithParams(uri: URI, params: Params): Either[HttpError, URI] = {
    val query = Request.buildParams(params)
    try {
      new URI(
        uri.getScheme, uri.getAuthority, uri.getPath, query, null
      ).right
    } catch {
      case ex: URISyntaxException =>
        HttpError.uriSyntaxError(
          s"""schema: ${uri.getScheme}
             |authority: ${uri.getAuthority}
             |path: ${uri.getPath}
             |query: $query""".stripMargin
        , ex
        ).left
    }
  }

  def toUrl(uri: URI): Either[HttpError, URL] =
    try {
      uri.toURL.right
    } catch {
      case _: IllegalArgumentException =>
        HttpError.nonAbsoluteUri(uri).left
      case ex: MalformedURLException =>
        HttpError.malformedUrl(uri, ex.getMessage).left
    }

  def openConnection(url: URL): Either[HttpError, URLConnection] =
    try {
      url.openConnection().right
    } catch {
      case ex: IOException =>
        HttpError.ioError(ex).left
    }

  def toHttpConnection(uri: URI): Either[HttpError, HttpConnection] =
    for {
      validUrl <- toUrl(uri)
      urlConnection <- openConnection(validUrl)
      response <- urlConnection match {
          case connection: HttpsURLConnection =>
            httpsConnection(connection).right
          case connection: HttpURLConnection =>
            httpConnection(connection).right
          case urlConnection: URLConnection =>
            HttpError.unsupportedUrlConnection(urlConnection).left
        }
    } yield response

  def setMethod(connection: HttpConnection, requestMethod: Request.Method): Unit =
    connection.httpUrlConnection.setRequestMethod(Request.Method.render(requestMethod))

  def setRequestHeaders(connection: HttpConnection, headers: Request.Headers): Unit = {
    for {
      (name, value) <- headers.headers
      if name.nonEmpty
      _ = connection.httpUrlConnection.setRequestProperty(name.toLowerCase, value)
    } yield ()
    ()
  }
}
