package just.http

/**
 * @author Kevin Lee
 * @since 2019-12-06
 */
object Constants {
  object Headers {
    val ContentDisposition: String = "Content-Disposition"
    val ContentType: String = "content-type"
    val ContentLength: String = "content-length"
  }
}
