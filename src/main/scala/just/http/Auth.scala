package just.http

import java.nio.charset.StandardCharsets
import java.util.Base64

/**
 * @author Kevin Lee
 * @since 2019-10-26
 */
sealed trait Auth

object Auth {
  final case class Basic(credentials: String) extends Auth

  def basic(credentials: String): Auth = Basic(credentials)
  def toBasic(username: String, password: String): Auth =
    basic(new String(Base64.getEncoder.encode(s"$username:$password".getBytes(StandardCharsets.UTF_8))))

  def toHeader(auth: Auth): (String, String) = auth match {
    case Basic(credentials) =>
      ("Authorization", s"Basic $credentials")
  }
}
