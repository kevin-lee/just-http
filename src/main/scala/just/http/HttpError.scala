package just.http

import java.io.IOException
import java.net.{URI, URISyntaxException, URLConnection}

/**
  * @author Kevin Lee
  * @since 2019-05-26
  */
sealed trait HttpError

object HttpError {
  final case class UnsupportedUrlConnection(urlConnection: URLConnection) extends HttpError
  final case class UriSyntaxError(uri: String, uriSyntaxException: URISyntaxException) extends HttpError
  final case class NonAbsoluteUri(uri: URI) extends HttpError
  final case class MalformedUrl(uri: URI, message: String) extends HttpError
  final case class IoError(ioException: IOException) extends HttpError
  final case class ServerError(nonFatal: Throwable) extends HttpError
  final case class UnsupportedMethod(request: Request) extends HttpError

  def unsupportedUrlConnection(urlConnection: URLConnection): HttpError =
    UnsupportedUrlConnection(urlConnection)

  def uriSyntaxError(uri: String, uriSyntaxException: URISyntaxException): HttpError =
    UriSyntaxError(uri, uriSyntaxException)

  def nonAbsoluteUri(uri: URI):  HttpError =
    NonAbsoluteUri(uri)

  def malformedUrl(uri: URI, message: String): HttpError =
    MalformedUrl(uri, message)

  def ioError(ioException: IOException): HttpError =
    IoError(ioException)

  def serverError(nonFatal: Throwable): HttpError =
    ServerError(nonFatal)

  def unsupportedMethod(request: Request): HttpError = UnsupportedMethod(request)


  def render(httpError: HttpError): String = httpError match {
    case UnsupportedUrlConnection(urlConnection) =>
      s"URLConnection is not supported. URLConnection: ${urlConnection.toString}"

    case UriSyntaxError(uri, uriSyntaxException) =>
      s"URI sytax error: URI: $uri - ${uriSyntaxException.getMessage}"

    case NonAbsoluteUri(uri) =>
      s"URI is not absolute. URI: ${uri.toString}"

    case MalformedUrl(uri, message) =>
      s"$message. URL: ${uri.toString}"

    case IoError(ioException) =>
      s"IOException: ${ioException.getMessage}"

    case ServerError(nonFatal) =>
      s"ServerError: ${nonFatal.getMessage}"

    case UnsupportedMethod(request) =>
      s"${Request.Method.render(request.method)} method is unsupported for this request: ${request.uri.toString}"
  }
}