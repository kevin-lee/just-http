package just.http

/**
  * @author Kevin Lee
  * @since 2019-05-25
  */
final case class Body(bytes: Array[Byte]) {
  override def equals(that: Any): Boolean = that match {
    case thatBytes: Array[Byte] =>
      bytes.sameElements(thatBytes)
    case _ =>
      false
  }
}
