package just.http

import argonaut._, Argonaut._

import just.fp._, syntax._

/**
 * @author Kevin Lee
 * @since 2019-10-04
 */
object Json {
  final case class Query(query: List[(String, String)]) extends AnyVal
  final case class Params(params: List[(String, String)]) extends AnyVal
  final case class Headers(headers: List[(String, String)]) extends AnyVal
  final case class Auth(authenticated: Boolean, user: String)

  object Query {
    def empty: Query = Query(List.empty)
  }
  object Params {
    def empty: Params = Params(List.empty)
  }
  object Headers {
    def empty: Headers = Headers(List.empty)
  }

  final case class Response(query: Query, params: Params, headers: Option[Headers])

  def decodeBody[A](body: Body, decoder: DecodeJson[A]): Either[String, A] =
    Parse.decodeEither(new String(body.bytes))(decoder)

  def decodeResponse(json: String): Either[String, Response] =
    Parse.decodeEither(json)(Json.responseDecoder)

  val responseDecoder: DecodeJson[Response] = DecodeJson(c => for {
    args <- (c --\ "args").as[Map[String, String]]
    form <- (c --\ "form").as[Option[Map[String, String]]]
    headers <- (c --\ "headers").as[Map[String, String]]
  } yield Response(Query(args.toList), Params(form.toList.flatMap(_.toList)), Headers(headers.toList).some))

  val authDecoder: DecodeJson[Json.Auth] = DecodeJson(c => for {
    authenticated <- (c --\ "authenticated").as[Boolean]
    user <- (c --\ "user").as[String]
  } yield Auth(authenticated, user))
}
