package just.http

import java.io.{ByteArrayOutputStream, File, FileInputStream}
import java.nio.file.Files

import scala.annotation.tailrec

/**
 * @author Kevin Lee
 * @since 2019-12-06
 */
object FileUtil {

  def loadFileToByteArray(path: File): Array[Byte] = {
    IoUtil.tryWith(new FileInputStream(path)) { input =>
      IoUtil.tryWith(new ByteArrayOutputStream()) { output =>
        IoUtil.transfer(32, input, output)
        output.toByteArray
      }
    }
  }

  def deleteRecursively(file: File): Unit = {
    @tailrec
    def go(files: Vector[File]): Boolean = files match {
      case x +: xs =>
        if (x.isDirectory) {
          val list = x.listFiles.toVector
          if (list.isEmpty)
            x.delete()
          else
            go(list ++ files)
        } else {
          x.delete()
          go(xs)
        }
      case Vector() =>
        true
    }
    if (file.isDirectory)
      go(file.listFiles.toVector :+ file)
    else
      file.delete()

    ()
  }

  def runWithCleanUp[A](prefix: String)(run: File => A): A = {
    val tmpDir = Files.createTempDirectory(prefix).toFile
    val closeable = new AutoCloseable {
      override def close(): Unit = deleteRecursively(tmpDir)
    }

    IoUtil.tryWith(closeable)(_ => run(tmpDir))
  }
}
