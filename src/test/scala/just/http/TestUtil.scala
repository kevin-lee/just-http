package just.http

import scala.concurrent.duration.{FiniteDuration, _}
import scala.concurrent.{Await, ExecutionContext, Future}

/**
 * @author Kevin Lee
 * @since 2019-05-26
 */
object TestUtil {

  private val atMost: FiniteDuration = 5.seconds

  @SuppressWarnings(Array("org.wartremover.warts.ImplicitParameter"))
  def getResponse[A](
    response: => Either[HttpError, A]
  )(implicit ex: ExecutionContext): Either[HttpError, A] =
    Await.result(Future(response), atMost)

}
