package just.http

import java.util.Base64

import hedgehog._
import hedgehog.runner._

/**
 * @author Kevin Lee
 * @since 2019-10-28
 */
object AuthSpec extends Properties {

  override def tests: List[Test] = List(
    property("test auth toBasic", testAuthToBasic)
  , property("test auth toHeader", testAuthToHeader)
  )

  def testAuthToBasic: Property = for {
    username <- Gen.string(Gen.alphaNum, Range.linear(5, 10)).log("username")
    password <- Gen.string(Gen.alphaNum, Range.linear(6, 26)).log("password")
  } yield {
    val auth = Auth.toBasic(username, password)
    auth match {
      case Auth.Basic(credentials) =>
        new String(Base64.getDecoder.decode(credentials)) ==== s"$username:$password"
    }
  }

  def testAuthToHeader: Property = for {
    username <- Gen.string(Gen.alphaNum, Range.linear(5, 10)).log("username")
    password <- Gen.string(Gen.alphaNum, Range.linear(6, 26)).log("password")
  } yield {
    val auth = Auth.toBasic(username, password)
    auth match {
      case Auth.Basic(credentials) =>
        Auth.toHeader(auth) match {
          case (name, value) =>
            name ==== "Authorization" and value ==== s"Basic $credentials"
        }
    }
  }
}
