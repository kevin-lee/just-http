package just.http

import java.nio.charset.{Charset, StandardCharsets}

import hedgehog._

import just.fp.syntax._

/**
 * @author Kevin Lee
 * @since 2019-09-16
 */
object Gens {

  val Utf8: Charset = StandardCharsets.UTF_8

  final case class ListLength(listLength: (Int, Int)) extends AnyVal

  def genPair[A, B](genA: Gen[A], genB: Gen[B]): Gen[(A, B)] = for {
    a <- genA
    b <- genB
  } yield (a, b)

  def genAsciiString(min: Int, max: Int): Gen[String] =
    Gen.string(Gen.ascii, Range.linear(min, max))

  def genUnicodeString(min: Int, max: Int): Gen[String] =
    Gen.string(Gen.unicodeAll, Range.linear(min, max))

  def genUnicodeStringWithCharset(charset: Charset, min: Int, max: Int): Gen[String] =
    Gen.string(Gen.unicodeAll, Range.linear(min, max)).map(s => new String(s.getBytes(charset)))

  def genDifferentStringPair: Gen[(String, String)] = for {
    x <- genUnicodeString(0, 100)
    y <- genUnicodeString(0, 100).map(z => if (x === z) "a" + z else z)
  } yield {
    (x, y)
  }

  def genUnicodeStringPair(length1: Int, length2: Int): Gen[(String, String)] =
    Gens.genPair(
        Gens.genUnicodeStringWithCharset(Utf8, 1, length1)
      , Gens.genUnicodeStringWithCharset(Utf8, 0, length2)
      )

  def genUniqueUnicodeStringPairs(
      length1: Int
    , length2: Int
    , listLength: ListLength
    ): Gen[List[(String, String)]] =
    Gens.genUnicodeStringPair(length1, length2)
      .list(Range.linear(listLength.listLength._1, listLength.listLength._2))
      .map(_.zipWithIndex.map { case ((k, v), index) => (s"${k}_${index.toString}", v) })

  def genAlphaNumStringPair(length1: Int, length2: Int): Gen[(String, String)] = for {
    k <- Gen.string(Gen.alphaNum, Range.linear(1, length1))
    v <- Gen.string(Gen.alphaNum, Range.linear(1, length2))
  } yield (k, v)

  def genUniqueAlphaNumStringPairs(
      length1: Int
    , length2: Int
    , listLength: ListLength
    ): Gen[List[(String, String)]] =
    Gens.genAlphaNumStringPair(length1, length2)
      .list(Range.linear(listLength.listLength._1, listLength.listLength._2))
      .map(_.zipWithIndex.map { case ((k, v), index) => (s"${k}_${index.toString}", v) })

  def genHeaders(
      length1: Int
    , length2: Int
    , listLength: ListLength
    ): Gen[List[(String, String)]] =
    genUniqueAlphaNumStringPairs(length1, length2, listLength)
    .map(_.map { case (name, value) =>
      (name.replaceAllLiterally("_", "-"), value)
    })

}
