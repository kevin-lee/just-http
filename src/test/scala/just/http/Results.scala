package just.http

import argonaut.DecodeJson
import hedgehog._

/**
 * @author Kevin Lee
 * @since 2019-10-04
 */
object Results {
  def assertResponse(actual: Body, expected: Json.Response): Result = {
    val bodyString = new String(actual.bytes)
    Json.decodeResponse(bodyString) match {
      case Right(Json.Response(query, params, maybeHeaders)) =>
        val querySorted = query.query.sorted
        val paramsSorted = params.params.sorted
        expected match {
          case Json.Response(expectedQuery, expectedParams, Some(expectedHeaders)) =>
            val expectedQuerySorted = expectedQuery.query.sorted
            val expectedParamsSorted = expectedParams.params.sorted
            Result.all(List(
                querySorted ==== expectedQuerySorted
              , paramsSorted ==== expectedParamsSorted
              , assertIfHeadersAreContained(maybeHeaders, expectedHeaders)

            ))
          case Json.Response(expectedQuery, expectedParams, None) =>
            val expectedQuerySorted = expectedQuery.query.sorted
            val expectedParamsSorted = expectedParams.params.sorted
            (querySorted ==== expectedQuerySorted).log(s"bodyString:\n${bodyString}") and (paramsSorted ==== expectedParamsSorted).log(s"bodyString:\n${bodyString}")
        }
      case Left(error) =>
        Result.failure.log(error)
    }
  }

  def assertResponseA[A](actual: Body, expected: A)(decoder: DecodeJson[A]): Result = {
    Json.decodeBody(actual, decoder) match {
      case Right(a) =>
        a ==== expected
      case Left(error) =>
        Result.failure.log(error)
    }
  }

  def assertHeaderNonEmpty(actual: Response): Result =
      Result.assert(actual.headers.headers.nonEmpty).log("headers are empty")

  @SuppressWarnings(Array("org.wartremover.warts.ToString"))
  def assertIfHeadersAreContained(actual: Option[Json.Headers], expected: Json.Headers): Result =
    (actual, expected) match {
      case (None, Json.Headers(Nil)) =>
        Result.success
      case (None, Json.Headers(_ :: _)) =>
        Result.failure.log("Expected headers not found")
        .log(s"actual: ${actual.toString}")
        .log(s"expected: ${expected.toString}")
      case (Some(actualHeaders), expected) =>
        val actualHeadersWithLowerCaseKeys = actualHeaders.headers.map(x => (x._1.toLowerCase, x._2))
        val expectedHeadersWithLowerCaseKeys = expected.headers.map(x => (x._1.toLowerCase, x._2))
        Result.diffNamed(
            "actual should contain all expected headers"
          , actualHeadersWithLowerCaseKeys
          , expectedHeadersWithLowerCaseKeys
          )((actual, expected) =>
            expected.forall(header => actual.contains(header))
          )
    }

}
