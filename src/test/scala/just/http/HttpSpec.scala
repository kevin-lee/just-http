package just.http

import java.io.File
import java.net.URLEncoder
import java.nio.charset.{Charset, StandardCharsets}

import hedgehog._
import hedgehog.runner._

import just.fp.syntax._

import just.http.Gens.ListLength
import just.http.Request.Params

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * @author Kevin Lee
  * @since 2019-05-26
  */
object HttpSpec extends Properties {
  val nTimes: Int = 100

  val hostname: String = sys.env.getOrElse("TEST_HOSTNAME", "localhost")

  def tests: List[Test] = List(
    example("test Http.get", testHttpGet)
  , property("test Http.get with params", testHttpGetWithParams).withTests(nTimes).noShrinking
  , property("test Http.getWithHeaders", testHttpGetWithHeaders).withTests(nTimes).noShrinking
  , property("test Http.getWithHeaders with params", testHttpGetWithHeadersAndParams).withTests(nTimes).noShrinking
  , property("test Http.get with basic auth", testHttpGetWithBasicAuth).withTests(nTimes).noShrinking
  , property("test download Http.get", testDownloadHttpGet)
  , example("test Http.post", testHttpPost)
  , property("test Http.post with params", testHttpPostWithParams).withTests(nTimes).noShrinking
  , property("test Http.postWithHeaders", testPostWithHeaders).withTests(nTimes).noShrinking
  , property("test Http.postWithHeaders with params", testPostWithHeadersAndParams).withTests(nTimes).noShrinking
  , example("test Http.put", testHttpPut)
  , property("test Http.put with params", testHttpPutWithParams).withTests(nTimes).noShrinking
  , property("test Http.putWithHeaders", testHttpPutWithHeaders).withTests(nTimes).noShrinking
  , property("test Http.putWithHeaders with params", testHttpPutWithHeadersAndParams).withTests(nTimes).noShrinking
  , example("test Http.delete", testHttpDelete)
  , property("test Http.delete with params", testHttpDeleteWithParams).withTests(nTimes).noShrinking
  , property("test Http.deleteWithHeaders", testHttpDeleteWithHeaders).withTests(nTimes).noShrinking
  , property("test Http.deleteWithHeaders with params", testHttpDeleteWithHeadersAndParams).withTests(nTimes).noShrinking
  , example("test Http.head", testHttpHead)
  , property("test Http.head with params", testHttpHeadWithParams).withTests(nTimes).noShrinking
  , property("test Http.headWithHeaders", testHttpHeadWithHeaders).withTests(nTimes).noShrinking
  , property("test Http.headWithHeaders with params", testHttpHeadWithHeadersAndParams).withTests(nTimes).noShrinking
  )

  val utf8: Charset = StandardCharsets.UTF_8

  def testHttpGet: Result = {
    val result = TestUtil.getResponse(Http.get(s"http://$hostname:8000/get", Params.empty))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, None))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpGetWithParams: Property = for {
    params <- Gens.genUniqueUnicodeStringPairs(5, 10, ListLength((0, 10)))
        .log("params")
  } yield {
    val query = Request.urlEncodeWithUtf8(params)
    val result = TestUtil.getResponse(Http.get(s"http://$hostname:8000/get", Params(params)))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params.empty, None))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpGetWithHeaders: Property = for {
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
  } yield {
    val result = TestUtil.getResponse(
      Http.getWithHeaders(s"http://$hostname:8000/get", Request.Headers(headers), Params.empty)
    )
    result match {
      case Right(res) =>
        Result.all(List(
          (res.status ==== Status(200)).log("status ==== 200")
        , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, Json.Headers(headers).some))
        ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpGetWithHeadersAndParams: Property = for {
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
    params <- Gens.genUniqueUnicodeStringPairs(5, 10, ListLength((0, 10)))
      .log("params")
  } yield {
    val query = Request.urlEncodeWithUtf8(params)
    val result = TestUtil.getResponse(
      Http.getWithHeaders(s"http://$hostname:8000/get", Request.Headers(headers), Params(params))
    )
    result match {
      case Right(res) =>
        Result.all(List(
          (res.status ==== Status(200)).log("status ==== 200")
        , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params.empty, Json.Headers(headers).some))
        ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpGetWithBasicAuth: Property = for {
    username <- Gen.string(Gen.alphaNum, Range.linear(5, 10)).log("username")
    password <- Gen.string(Gen.alphaNum, Range.linear(6, 26)).log("password")
  } yield {
    val url = s"http://$hostname:8000/basic-auth/$username/$password"

    (for {
      request <- Request.get(url)
      requestWithAuth =  Request.withAuth(request, Auth.toBasic(username, password))
      result <- TestUtil.getResponse(
        Http.request(requestWithAuth, Params.empty)
      )
    } yield result) match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponseA(res.body, Json.Auth(true, username))(Json.authDecoder)
        ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }

  }

  def testDownloadHttpGet: Property = for {
    n <- Gen.int(Range.linear(1, 256)).log("n")
    filename <- Gen.string(Gen.alphaNum, Range.linear(5, 10)).log("filename")
  } yield FileUtil.runWithCleanUp("just-http-download-") { tmpDir =>
    (for {
      uri <- Connection.uri(s"http://$hostname:8000/bytes/${n.toString}")
      request = Request(uri, Request.Method.get, Request.Headers.empty)
      response <- TestUtil.getResponse(Http.download(request, Params.empty) {
            (uri, headers, input) =>
              Download.download(Download.FileDownload(tmpDir, filename.some), uri, headers, 32)(input)
          })
    } yield response) match {
      case Right(res) =>
        val actual = FileUtil.loadFileToByteArray(res)
        val expected = FileUtil.loadFileToByteArray(new File(tmpDir, filename))
        Result.all(List(
            Result.diffNamed(s"Response Array[Byte].length ==== ${n.toString}", actual.length, n)(_ === _)
          , Result.diffNamed("actual ==== expected", actual, expected)(_.sameElements(_))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpPost: Result = {
    val result = TestUtil.getResponse(Http.post(s"http://$hostname:8000/post", Params.empty))

    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, None))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpPostWithParams: Property = for {
    query <- Gens.genUniqueUnicodeStringPairs(8, 10, ListLength((0, 5)))
      .log("query")
    params <- Gens.genUniqueUnicodeStringPairs(10, 20, ListLength((0, 10)))
      .log("params")
  } yield {
    val result = TestUtil.getResponse(Http.post(s"http://$hostname:8000/post${toQueryString(query)}", Params(params)))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params(params), None))
          ))
      case Left(error) =>
        Result.failure.log(s"Left(error: ${HttpError.render(error)})")
    }

  }

  def testPostWithHeaders: Property = for {
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
  } yield {
    val result = TestUtil.getResponse(
      Http.postWithHeaders(s"http://$hostname:8000/post", Request.Headers(headers), Params.empty)
    )
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, Json.Headers(headers).some))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testPostWithHeadersAndParams: Property = for {
    query <- Gens.genUniqueUnicodeStringPairs(8, 10, ListLength((0, 5)))
      .log("query")
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
    params <- Gens.genUniqueUnicodeStringPairs(10, 10, ListLength((0, 10)))
      .log("params")
  } yield {
    val result = TestUtil.getResponse(
      Http.postWithHeaders(s"http://$hostname:8000/post${toQueryString(query)}", Request.Headers(headers), Params(params))
    )
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params(params), Json.Headers(headers).some))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpPut: Result = {
    val result = TestUtil.getResponse(Http.put(s"http://$hostname:8000/put", Params.empty))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, None))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpPutWithParams: Property = for {
    query <- Gens.genUniqueUnicodeStringPairs(8, 10, ListLength((0, 5)))
      .log("query")
    params <- Gens.genUniqueUnicodeStringPairs(10, 10, ListLength((0, 10)))
      .log("params")
  } yield {

    val result = TestUtil.getResponse(Http.put(s"http://$hostname:8000/put${toQueryString(query)}", Params(params)))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params(params), None))
          ))
      case Left(error) =>
        Result.failure.log(s"Left(error: ${HttpError.render(error)})")
    }
  }

  def testHttpPutWithHeaders: Property = for {
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
  } yield {
    val result = TestUtil.getResponse(
      Http.putWithHeaders(s"http://$hostname:8000/put", Request.Headers(headers), Params.empty)
    )
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, Json.Headers(headers).some))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpPutWithHeadersAndParams: Property = for {
    query <- Gens.genUniqueUnicodeStringPairs(8, 10, ListLength((0, 5)))
      .log("query")
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
    params <- Gens.genUniqueUnicodeStringPairs(10, 10, ListLength((0, 10)))
      .log("params")
  } yield {
    val result = TestUtil.getResponse(
      Http.putWithHeaders(s"http://$hostname:8000/put${toQueryString(query)}", Request.Headers(headers), Params(params))
    )
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params(params), Json.Headers(headers).some))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpDelete: Result = {
    val result = TestUtil.getResponse(Http.delete(s"http://$hostname:8000/delete", Params.empty))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, None))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpDeleteWithParams: Property = for {
    query <- Gens.genUniqueUnicodeStringPairs(8, 10, ListLength((0, 5)))
      .log("query")
    params <- Gens.genUniqueUnicodeStringPairs(10, 10, ListLength((0, 10)))
      .log("params")
  } yield {
    val result = TestUtil.getResponse(Http.delete(s"http://$hostname:8000/delete${toQueryString(query)}", Params(params)))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params(params), None))
          ))
      case Left(error) =>
        Result.failure.log(s"Left(error: ${HttpError.render(error)})")
    }
  }

  def testHttpDeleteWithHeaders: Property = for {
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
  } yield {
    val result = TestUtil.getResponse(
      Http.deleteWithHeaders(s"http://$hostname:8000/delete", Request.Headers(headers), Params.empty)
    )
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query.empty, Json.Params.empty, Json.Headers(headers).some))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpDeleteWithHeadersAndParams: Property = for {
    query <- Gens.genUniqueUnicodeStringPairs(8, 10, ListLength((0, 5)))
      .log("query")
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
    params <- Gens.genUniqueUnicodeStringPairs(10, 10, ListLength((0, 10)))
      .log("params")
  } yield {
    val result = TestUtil.getResponse(
      Http.deleteWithHeaders(s"http://$hostname:8000/delete${toQueryString(query)}", Request.Headers(headers), Params(params))
    )
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Results.assertResponse(res.body, Json.Response(Json.Query(query), Json.Params(params), Json.Headers(headers).some))
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpHead: Result = {
    val result = TestUtil.getResponse(Http.head(s"http://$hostname:8000/get", Params.empty))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Result.assert(res.body.bytes.isEmpty)
          , Results.assertHeaderNonEmpty(res)
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpHeadWithParams: Property = for {
    params <- Gens.genUniqueUnicodeStringPairs(5, 5, ListLength((0, 10)))
      .log("params")
  } yield {
    val result = TestUtil.getResponse(Http.head(s"http://$hostname:8000/get", Params(params)))
    result match {
      case Right(res) =>
        Result.all(List(
            (res.status ==== Status(200)).log("status ==== 200")
          , Result.assert(res.body.bytes.isEmpty)
          , Results.assertHeaderNonEmpty(res)
          ))

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpHeadWithHeaders: Property = for {
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
  } yield {
    val result = TestUtil.getResponse(
      Http.headWithHeaders(s"http://$hostname:8000/get", Request.Headers(headers), Params.empty)
    )
    result match {
      case Right(res) =>
        (res.status ==== Status(200)).log("status ==== 200")

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  def testHttpHeadWithHeadersAndParams: Property = for {
    query <- Gens.genUniqueUnicodeStringPairs(8, 10, ListLength((0, 5)))
      .log("query")
    headers <- Gens.genHeaders(5, 5, ListLength((0, 10)))
      .log("headers")
    params <- Gens.genUniqueUnicodeStringPairs(10, 10, ListLength((0, 5)))
      .log("params")
  } yield {
    val result = TestUtil.getResponse(
      Http.headWithHeaders(s"http://$hostname:8000/get${toQueryString(query)}", Request.Headers(headers), Params(params))
    )
    result match {
      case Right(res) =>
        (res.status ==== Status(200)).log("status ==== 200")

      case Left(error) =>
        Result.failure.log(s"error: ${HttpError.render(error)}")
    }
  }

  private def toQueryString(query: List[(String, String)]): String = {
    val queryString = (for {
      (name, value) <- query
      if name.nonEmpty
      encodedName = URLEncoder.encode(name, "UTF-8").replaceAllLiterally("+", "%20")
      encodedValue = URLEncoder.encode(value, "UTF-8").replaceAllLiterally("+", "%20")
    } yield s"$encodedName=$encodedValue").mkString("&")
    if (queryString.isEmpty) "" else s"?$queryString"
  }

  private def assertHead(res: Response, expected: List[(String, List[String])]): List[Result] = {
    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    val theNull: String = null
    List(
        (res.status ==== Status(200)).log("status ==== 200")
      , (res.body.bytes.length ==== 0).log(s"body length ==== 0")
      , Result.diffNamed(
        "actual contains all expected"
        , res.headers.headers.map {
          case (`theNull`, vs) =>
            (theNull, vs)
          case (k, vs) =>
            (k.toLowerCase(), vs.sorted)
        }
        , expected.map { case (k, vs) => (k.toLowerCase(), vs.sorted) }
      )(
        (actual, expectedHeaders) =>
          expectedHeaders.forall(actual.contains)
      )
    )
  }

}
