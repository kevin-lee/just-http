package just.http

/**
 * @author Kevin Lee
 * @since 2019-09-16
 */
object CompatUtil {
  def mapValues[A, B, C](map: Map[A, B])(f: B => C): Map[A, C] =
    map.view.mapValues(f).toMap
}
