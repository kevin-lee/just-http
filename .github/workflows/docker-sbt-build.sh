#!/bin/bash -e

set -x

if [ -z "$1" ]
  then
    echo "Scala version is missing. Please enter the Scala version."
    echo "sbt-build.sh 2.11.12"
    exit 1
else
  SCALA_VERSION=$1
  echo "============================================"
  echo "Build projects"
  echo "--------------------------------------------"
  echo ""
  cd /app
  export CI_BRANCH="${GITHUB_REF#refs/heads/}"
  if [[ "$CI_BRANCH" == "master" || "$CI_BRANCH" == "release" ]]
  then
    sbt -J-Xmx2048m "; ++ ${SCALA_VERSION}!; clean; coverage; test; coverageReport; coverageAggregate"
    sbt -J-Xmx2048m "; ++ ${SCALA_VERSION}!; coveralls"
    sbt -J-Xmx2048m "; ++ ${SCALA_VERSION}!; clean; packagedArtifacts"
  else
    sbt -J-Xmx2048m "; ++ ${SCALA_VERSION}!; clean; coverage; test; coverageReport; coverageAggregate; package"
    sbt -J-Xmx2048m "; ++ ${SCALA_VERSION}!; coveralls"
  fi


  echo "============================================"
  echo "Building projects: Done"
  echo "============================================"
fi
