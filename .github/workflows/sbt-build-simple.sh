#!/bin/bash -e

set -x

if [ -z "$1" ]
  then
    echo "Scala version is missing. Please enter the Scala version."
    echo "sbt-build-simple.sh 2.11.12"
    exit 1
else
  SCALA_VERSION=$1
  echo "============================================"
  echo "Build projects (Simple)"
  echo "--------------------------------------------"
  echo ""
  export CI_BRANCH="${GITHUB_REF#refs/heads/}"
  if [[ "$CI_BRANCH" == "master" || "$CI_BRANCH" == "release" ]]
  then
    sbt -J-Xmx2048m "; ++ ${SCALA_VERSION}!; clean; test; packagedArtifacts"
  else
    sbt -J-Xmx2048m "; ++ ${SCALA_VERSION}!; clean; test; package"
  fi

  echo "============================================"
  echo "Building projects (Simple): Done"
  echo "============================================"
fi
